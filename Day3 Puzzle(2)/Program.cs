﻿using System;
using System.IO;

namespace Day3_Puzzle_2_
{
    class Program
    {
        public void createFile()
        {
             FileStream f = new FileStream(@"C:\Users\shruti.kashid\C# Project\Day3 Puzzle(2)\Sample.txt",FileMode.OpenOrCreate);
           StreamWriter s = new StreamWriter(f);
           s.WriteLine("Hello World");
           s.Close();
           f.Close();
           Console.WriteLine("File is Created !");

        }
        public void readFile()
        {
             FileStream f = new FileStream(@"C:\Users\shruti.kashid\C# Project\Day3 Puzzle(2)\Sample.txt",FileMode.OpenOrCreate);
           StreamReader s = new StreamReader(f);
           string line = s.ReadLine();
           Console.WriteLine("How many character you want to print : ");
           int n =Convert.ToInt32(Console.ReadLine());
           for(int i=0;i<n;i++)
           {
               Console.Write(line[i]);
           }
           
           s.Close();
           f.Close();
        }
        public static void Main(string []args)
        {
            Program p = new Program();
           // p.createFile();
            p.readFile();
        }
          
        
    }
}
