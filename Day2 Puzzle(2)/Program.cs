﻿/*Given a array of numbers representing the stock prices of a company in chronological order,
 write a function that calculates the maximum profit you 
could have made from buying and selling that stock once. You must buy before you can sell it.
For example, given [9, 11, 8, 5, 7, 10], you should return 5, 
since you could buy the stock at 5 dollars and sell it at 10 dollars.*/
using System;
namespace Day2_Puzzle_2_
{
    class Program
    {
        int getMaxProfit(int[] stockPrices)
        {
            int lowPrice = stockPrices[0];
            int MaxProfit = int.MinValue;
            for(int i=0;i<stockPrices.Length;i++)
            {
                int currentProfit=0;
                if(stockPrices[i]>lowPrice)
                {
                    currentProfit=stockPrices[i]-lowPrice;
                    if(currentProfit>MaxProfit)
                    {
                        MaxProfit=currentProfit;
                    }
                }
                else
                {
                    lowPrice=stockPrices[i];
                }
            }
            return MaxProfit;
        }
        public static void Main(String [] args)
        {
            Program p = new Program();
            int[] temp={9, 11, 8, 5, 7, 10};
            Console.WriteLine("Maximum Profit is : "+ p.getMaxProfit(temp));
        }
    }
}
