﻿using System;
using System.Collections.Generic;

public class TimeStamp
{
    public int timestamp;
    public int count;
    public string type;
    public  TimeStamp(int timestamp,int count,string type)
    {
        this.timestamp=timestamp;
        this.count=count;
        this.type=type;
    }
}
public class Program
{
    public static void Main(String []args)
    {
        TimeStamp ts1 = new TimeStamp(12345345,3,"Enter");
         TimeStamp ts2 = new TimeStamp(12345348,2,"Exit");
          TimeStamp ts3 = new TimeStamp(12345328,4,"Enter");
           TimeStamp ts4 = new TimeStamp(12345367,1,"Exit");
            TimeStamp ts5 = new TimeStamp(12345367,4,"Exit");

            List<TimeStamp> list = new List<TimeStamp>();
            list.Add(ts1);
            list.Add(ts2);
            list.Add(ts3);
            list.Add(ts4);
            list.Add(ts5);

            int count=0;
            int currentTimeStamp=0;
            int maxTimeStamp=0;
            int maxCount=0;

            for(int i=0;i<list.Count;i++)
            {
                if(list[i].timestamp !=currentTimeStamp)
                {
                    if(count>maxCount)
                    {
                        maxTimeStamp=currentTimeStamp;
                        maxCount=count;
                    }
                    currentTimeStamp=list[i].timestamp;

                }
                if(list[i].type=="Enter")
                {
                    count=count+list[i].count;
                }
                else
                {
                    count=count-list[i].count;
                }
            }
            if(count>maxCount)
            {
                maxTimeStamp=currentTimeStamp;
                maxCount=count;
            }
            Console.WriteLine(count);
            Console.WriteLine("Busiest period in building : "+maxTimeStamp);



    }
}