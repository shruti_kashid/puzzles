﻿using System;
using System.Collections.Generic;
namespace Puzzle
{
    public class Program
    {
        public class Node
        {
              public int data;    
             public Node next;    

             public Node(int data)
             {
                 this.data=data;
                 this.next=null;
             }
        }
     // public Node head=null;
        public Node tail=null;
        public Node pushNode(Node head,int data)
        {
            
            Node newnode=new Node(data);
            if(head==null)
            {
                head=newnode;
                tail=newnode;

            }
            else
            {
                tail.next=newnode;
              tail=newnode;
            }
            return head;
        }
        public int intersect(Node head1,Node head2)
        {
            Node current1=head1;
            Node current2=head2;
                int result=0;

            while(current1 !=null && current2 !=null)
            {
                if (current1.data == current2.data) {
                 result= current1.data;
            }
            current1=current1.next;
            current2=current2.next;
            }
            return result;

        }
        
        public void display(Node head)
        {
            Node current = head;    
            
        if(head == null) {    
           Console.WriteLine("List is empty");    
            return;    
        }    
        Console.WriteLine("Nodes of singly linked list: ");    
        while(current != null) {    
            
           Console.Write(current.data + " ");    
            current = current.next;    
        }    
      Console.WriteLine();    
    }    
       
    public static void Main(String[] args) {    
            Node root1 = null, root2 = null;
        Program p = new Program();   
            
         
        root1=p.pushNode(root1,1);    
         root1=p.pushNode(root1,2);    
         root1=p.pushNode(root1,3);    
         root1=p.pushNode(root1,4);    
         root1=p.pushNode(root1,5);    
            
        //Displays the nodes present in the 1St list  
        Console.WriteLine("First List");  
        p.display(root1);

        root2=p.pushNode(root2,11);    
         root2=p.pushNode(root2,21);    
         root2=p.pushNode(root2,31);    
        root2=p.pushNode(root2,4);    
            
        //Displays the nodes present in the 1St list  
       Console.WriteLine("Second List");  
        p.display(root2);    

        Console.WriteLine("Intersecting node between two linked list is : ");
        Console.WriteLine(p.intersect(root1,root2));
        }

        
    }
}
