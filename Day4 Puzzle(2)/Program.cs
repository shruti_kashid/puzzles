﻿using System;

namespace Day3_Puzzle_3_
{
    
        public class Node
        {
            public int data;
            public Node left;
            public Node right;
            public Node(int data)
            {
                this.data=data;
                this.left=null;
                this.right=null;

               }
       class Program
       {
           Node root;
           int value;
           int deeplevel;
           public void traverseBinaryTree(Node node)
           {
               if(node != null)
               {
                   traverseBinaryTree(node.left);
                   Console.Write(" "+node.data);
                   traverseBinaryTree(node.right);

               }
           }
           public int MinimumPathSum(Node node)
           {
               if(node==null)
               {
                   return 0 ;
               }
               int sum = node.data;
               int leftTreeSum =MinimumPathSum(node.left);
               int rightTreesum=MinimumPathSum(node.right);

               if(leftTreeSum>rightTreesum)
               {
                   sum =sum+leftTreeSum;

               }
               else{
                   sum=sum+rightTreesum;

               }
              // Console.WriteLine("Minimum path sum : "+sum);
              return sum;
           }
           
           
           public static void Main(String []args)
           {
               Program p = new Program();
               p.root = new Node(2);
               p.root.left= new Node(1);
               p.root.right = new Node(4);
                p.root.left.right= new Node(1);
                p.root.left.left= new Node(4);
                Console.WriteLine("Binary tree : ");
                p.traverseBinaryTree(p.root);
                Console.WriteLine();
                Console.WriteLine("Minimum Path Sum : "+p.MinimumPathSum(p.root));
              


           }

       }
        
    }
}
