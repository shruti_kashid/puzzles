﻿using System;

namespace Day3_Puzzle_1_
{
    class Program
    {
        public class Node
        {
              public int data;    
             public Node next;    

             public Node(int data)
             {
                 this.data=data;
                 this.next=null;
             }
        }
     // public Node head=null;
        public Node tail=null;
        public Node pushNode(Node head,int data)
        {
            
            Node newnode=new Node(data);
            if(head==null)
            {
                head=newnode;
                tail=newnode;

            }
            else
            {
                tail.next=newnode;
              tail=newnode;
            }
            return head;
        }
        public Node ReverseLinkedList(Node head )
        {
            Node next = null;
            Node previous = null;
            Node current=head;
            while(current!=null)
            {
                next=current.next;
                current.next=previous;
                previous=current;
                current=next;

            }
            return previous;
           
        }
        public void display(Node head)
        {
            Node current = head;    
            
        if(head == null) {    
           Console.WriteLine("List is empty");    
            return;    
        }    
        Console.WriteLine("Nodes of singly linked list: ");    
        while(current != null) {    
            
           Console.Write(current.data + " ");    
            current = current.next;    
        }    
      Console.WriteLine();    
    }
    public static void Main(String []args)
    {
        Node root1 = null;
        Program p = new Program();   
            
         
        root1=p.pushNode(root1,1);    
         root1=p.pushNode(root1,2);    
         root1=p.pushNode(root1,3);    
         root1=p.pushNode(root1,4);    
         root1=p.pushNode(root1,5);    
            
        //Displays the nodes present in the 1St list  
        Console.WriteLine("First List");  
        p.display(root1);
        
        root1=p.ReverseLinkedList(root1);
         Console.WriteLine("Reverse  List");  
       
    p.display(root1);

    }
      

    }
}
