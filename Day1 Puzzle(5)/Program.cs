﻿using System;

namespace Puzzle_Day1_5_
{
    class Program
    {
        public void print(int[] start , int[] end)
        {
           Console.WriteLine("Start Time of lecture ");
           for(int i=0;i<start.Length;i++)
           {
               Console.WriteLine(start[i]);
           }    
           Console.WriteLine("End Time of lecture ");
           for(int i=0;i<end.Length;i++)
           {
               Console.WriteLine(end[i]);
           }    
        }
        public void findMinimumRoom(int[] starttime , int[] endtime)
        {
            Array.Sort(starttime);
            Array.Sort(endtime);
           int count=1;
           int room=1;
           int i=1;
           int j=0;
           while(i<starttime.Length && j<starttime.Length)
           {
               if(starttime[i]<endtime[j])
               {
                   count++;
                   if(count>room)
                   {
                       room=count;
                      
                 }
                    i++;
               }

                   else
                   {
                       count--;
                       j++;
                   }
               }
               Console.WriteLine("Minimum required class rooms : "+room);  
          }
           public static void Main(string []args)
        {
            Program p = new Program();
            
            Console.WriteLine("Enter the count of lectures : ");
            int r=Convert.ToInt32(Console.ReadLine());
            int[] startTemp = new int[r];
            int[] endTemp = new int[r];
            Console.WriteLine("Enter the start and end time of lectures : ");
            for(int i=0;i<r;i++)
            {
                Console.WriteLine("Enter Start Time : ");
                startTemp[i]=Int32.Parse(Console.ReadLine());
                

            }
            for(int j=0;j<r;j++)
                {
                    Console.WriteLine("Enter End Time : ");
                    endTemp[j]=Int32.Parse(Console.ReadLine());
                    
                }
            p.print(startTemp,endTemp);
            p.findMinimumRoom(startTemp,endTemp);
            
        }
    }       
    
}