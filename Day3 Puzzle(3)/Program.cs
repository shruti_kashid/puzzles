﻿using System;

namespace Day3_Puzzle_3_
{
    
        public class Node
        {
            public int data;
            public Node left;
            public Node right;
            public Node(int data)
            {
                this.data=data;
                this.left=null;
                this.right=null;

               }
       class Program
       {
           Node root;
           int value;
           int deeplevel;
           public void traverseBinaryTree(Node node)
           {
               if(node != null)
               {
                   traverseBinaryTree(node.left);
                   Console.Write(" "+node.data);
                   traverseBinaryTree(node.right);

               }
           }
           public void find(Node root ,int level)
           {
               if(root!=null)
               {
                   find(root.left,++level);
                   if(level>deeplevel)
                   {
                       value=root.data;
                       deeplevel=level;
                   }
                   find(root.right,level);
               }
           }
           public int findLastLeafNode(Node root)
           {
               find(root,0);
               return value;
           }
           public static void Main(String []args)
           {
               Program p = new Program();
               p.root = new Node(1);
               p.root.left= new Node(3);
               p.root.right = new Node(5);
               p.root.left.left= new Node(7);
                p.root.left.right= new Node(9);
                p.root.left.left.left= new Node(10);
                Console.WriteLine("Binary tree : ");
                p.traverseBinaryTree(p.root);
                Console.WriteLine();
                Console.WriteLine("Deepest Node is  : "+p.findLastLeafNode(p.root));



           }

       }
        
    }
}
