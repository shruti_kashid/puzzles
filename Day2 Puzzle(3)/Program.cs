﻿/*A number is considered perfect if its digits sum up to exactly 10.
Given a positive integer n, return the n-th perfect number.
For example, given 1, you should return 19. Given 2, you should return 28.*/



using System;

namespace Day2_Puzzle_3_
{
    class Program
    {
        
            int findPerfectNumber(int num)
            {
                int count=0;
            
           
           for(int i=1;;i++)
           {

           
               int SumOfnumber=0;                
            for (int x = i; x > 0; x = x / 10) 
            {
                      SumOfnumber = SumOfnumber + x % 10; 
                      
            }
               
               if(SumOfnumber==10)
               {
                   count++;

               }
               if(count==num)
               {
                   return i;
               }

            }
           
        }
        public static void Main(String []args)
        {
            Program p = new Program();
            Console.WriteLine("Enter any number ");
            int temp = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Perfect number is : "+p.findPerfectNumber(temp));
        }
    }
}
